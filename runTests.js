const path = require('path'),
    async = require('async'),
    newman = require('newman'),
    fs = require('fs'),
    recursive = require("recursive-readdir"),
    handlebars = require('handlebars'),
    winston = require('winston'),
    config = require(path.join(__dirname, 'custom', 'config.json')),
    collection = path.join(__dirname, 'custom', config.collection),
    environmentFolder = path.join(__dirname, 'custom', 'environments'),
    globalTemplate = path.join(__dirname, 'templates', 'report_global.hbs'),
    envTemplate = path.join(__dirname, 'templates', 'report_environment.hbs'),
    libsFolder = 'libs',
    customFolder = 'custom',
    destinationDir = config.destinationDir;

var fileNames = [];

/**
 * Closure for collection runner with needed options.
 * @param {String} file Postman environment filepath.
 */
function collectionRun(file) {
	
	var newman = require('newman');
	
    var envFile = file.substring(file.lastIndexOf('/') + 1),
        envName = envFile.substring(0, envFile.indexOf('.')),
        fileName = `${envName}.html`

    fileNames.push(fileName);

    return done => {
		logger.info('collection ... ' + collection);
		logger.info('envFile ... ' + envFile);
        logger.info('Executing ... ' + envName);
		logger.info('Start newman run ... ');
		
        newman.run({
            collection: collection,
            environment: file,
            timeout: config.timeout,
            insecure: true,
            reporters: ['html'],
            reporter: {
                html: {
                    export: path.join(destinationDir, fileName),
                    template: envTemplate
                }
            }
        }, (err, summary) => {
            if (err) { logger.error(err); }
            done(null, summary);
        });
		
		/*newman.run({
			collection: 'C:\Users\test\endpoints-health-checker\custom\pet_store.postman_collection.json',
			environment:'C:\Users\test\endpoints-health-checker\custom\environments\petstore.postman_environment.json',
			reporters: ['html'],
            reporter: {
                html: {
                    export: path.join(destinationDir, fileName),
                    template: envTemplate
                }
            }
		}, function (err) {
			if (err) { logger.error(err); }
            done(null, summary);
			console.log('collection run complete!');
		});*/
		
		/*newman.run({
		   collection: 'C:\Users\test\endpoints-health-checker\custom\pet_store.postman_collection.json',
		   environment: 'C:\Users\test\endpoints-health-checker\custom\environments\petstore.postman_environment.json',
		   timeout: config.timeout,
           insecure: true,
           reporters: ['html'],
           reporter: {
                html: {
                    export: path.join(destinationDir, fileName),
                    template: envTemplate
                }
            }
		}(err, summary) => {
            if (err) { logger.error(err); }
            done(null, summary);
        });*/
    }
}

/**
 * Copies JS, CSS and other resource files to destination folder
 * @param {*} source Source folder path
 */
function copyResources(source) {
    recursive(source, (err, files) => {
        const regex = new RegExp('.*\.(js|css|png|jpg|jpeg|woff2)', 'gi');

        files.filter(file => file.match(regex))
            .forEach(file => fs.createReadStream(file).pipe(fs.createWriteStream(path.join(destinationDir, path.basename(file)))))
    });
}

// Logger config
const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
      new winston.transports.File({ filename: 'combined.log' })
    ]
  }).add(new winston.transports.Console({
    format: winston.format.simple()
}));


// Proxy
if (config.proxy) {
    logger.info('Using proxy ' + config.proxyEndpoint);
    process.env.http_proxy = config.proxyEndpoint;
    process.env.https_proxy = config.proxyEndpoint;
}

// Copy resources in dest folder
!fs.existsSync(destinationDir) && fs.mkdirSync(destinationDir)
copyResources(libsFolder);
copyResources(customFolder);

// Gets environments files
recursive(environmentFolder, (err, files) => {
    if (err) { throw err; }

    var collectionRuns = [];

    // Add colletion runners for each environment to an array
    files.filter(file => file.endsWith('.postman_environment.json'))
        .forEach(file => collectionRuns.push(collectionRun(file))); 

    // Exec async collection
    async[config.runType](collectionRuns, (err, results) => {
        if (err) { 
            // Some error appeared, maybe is a JS error in Postman script test
            logger.error(err); 
        }

        // Get global template
        fs.readFile(globalTemplate, 'utf8', (err, data) => {
            if (err) {
                logger.error(err);
                throw err;
            }

            var template = handlebars.compile(data),
                context = {
                    results: []
                };

            results.forEach((result, index) => {
                if (!result) return;

                var object = {
                    name: result.environment.name,
                    failures: result.run.failures.length,
                    fileName: fileNames[index]
                };

                context.results.push(object);
            });

            // Print results in html file
            fs.writeFile(path.join(destinationDir, 'results.html'), template(context), 'utf8', err => {
                if (err) { throw err; }
                process.exit(0);
            })
          });
    });
});
