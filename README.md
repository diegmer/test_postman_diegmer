# Endpoints Health Checker

#### Configure it
- In custom/config.json set the configuration for your test:
    - collection: name of postman collection inside custom folder. Example: "example.postman_collection.json
    - proxy: true if a proxy has to be used
    - proxyEndpoint: if proxy=true, endpoint to be used as proxy. Example: http://10.108.36.10:8080
    - destinationDir: folder where results where saved
    - runType: series | parallel
    - timeout: miliseconds to exit tests if no response in that time
- All CSS/styles can be changed in custom/styles.css
- Tool logo image can be changed: custom/logo.png
- In custom/ folder any resource can be added to be added in the results

#### Install dependences
> npm install

#### Run
> node runTests.js

#### Other
Run linter
> npm run lint
